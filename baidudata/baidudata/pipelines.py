# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.pipelines.images import ImagesPipeline
from scrapy.http import Request
import re
import uuid

class BaidudataPipeline(ImagesPipeline):
    CONVERTED_ORIGINAL = re.compile('^full/[0-9,a-f]+.jpg$')

    def get_media_requests(self, item, info):
        print item['url']
        yield Request(item['url'], meta={'word': item['word']})

    def get_images(self, response, request, info):
        for key, image, buf, in super(BaidudataPipeline, self).get_images(response, request, info):
            if self.CONVERTED_ORIGINAL.match(key):
                key = self.change_filename(key, response)
            yield key, image, buf
    def change_filename(self, key, response):
        return response.meta['word'] + "/%s.jpg" % str(uuid.uuid4())
