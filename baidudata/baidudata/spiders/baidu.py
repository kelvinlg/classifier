# -*- coding: utf-8 -*-
import scrapy
import json
import urllib
from urllib import urlencode
import uuid
from scrapy.item import Item, Field

class CustomItem(Item):
    url = Field()
    word = Field()

class BaiduSpider(scrapy.Spider):
    name = "baidu"
    #allowed_domains = ["baidu.com"]
    #start_urls = ['http://baidu.com/']

    def new_request(self, word, page, callback):
        headers={
            'Accept-Encoding': 'deflate, sdch, br',
            'Accept-Language': 'en-US,en;q=0.8',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
            'Accept': 'text/plain, */*; q=0.01',
            'Referer': 'https://image.baidu.com/search/index?tn=baiduimage&ipn=r&ct=201326592&cl=2&lm=-1&st=-1&fm=result&fr=&sf=1&fmq=1495945010972_R&pv=&ic=0&nc=1&z=&se=1&showtab=0&fb=0&width=&height=&face=0&istype=2&ie=utf-8&word=' + word,
            'X-Requested-With': 'XMLHttpRequest',
            'Connection': 'keep-alive',
        }
        params = (
            ('tn', 'resultjson_com'),
            ('ipn', 'rj'),
            ('ct', '201326592'),
            ('is', ''),
            ('fp', 'result'),
            ('queryWord', word),
            ('cl', '2'),
            ('lm', '-1'),
            ('ie', 'utf-8'),
            ('oe', 'utf-8'),
            ('adpicid', ''),
            ('st', '-1'),
            ('z', ''),
            ('ic', '0'),
            ('word', word),
            ('s', ''),
            ('se', ''),
            ('tab', ''),
            ('width', ''),
            ('height', ''),
            ('face', '0'),
            ('istype', '2'),
            ('qc', ''),
            ('nc', '1'),
            ('fr', ''),
            ('pn', page),
            ('rn', '60'),
            ('gsm', '1e'),
            ('1495945066638', ''),
        )
        cookies = {
            'BDUSS': '16dnYzcGc0NnJHZGI2SWZ5QzVxMnlIMkNpMGVBZjBWN2NYb3RwZzRUM2pmN3BYQVFBQUFBJCQAAAAAAAAAAAEAAACJi0Y2y6zAyrXEwbq80s6wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOPyklfj8pJXWW',
            'BAIDUID': '9716820A0D53AEDA39662CEBC5D2A5C3:FG=1',
            'BIDUPSID': '9716820A0D53AEDA39662CEBC5D2A5C3',
            'PSTM': '1495944993',
            'PSINO': '7',
            'H_PS_PSSID': '22164_1450_21078_20929',
            'BDORZ': 'B490B5EBF6F3CD402E515D22BCDA1598',
            'BDRCVFR[-pGxjrCMryR]': 'mk3SLVN4HKm',
            'indexPageSugList': '%5B%22test%22%5D',
            'cleanHistoryStatus': '0',
            'BDRCVFR[dG2JNJb_ajR]': 'mk3SLVN4HKm',
            'userFrom': 'www.baidu.com',
        }
        meta = {
            'queryWord': word
        }
        return scrapy.Request(
            'https://image.baidu.com/search/acjson?' + urlencode(params),
            method='GET',
            headers=headers,
            cookies=cookies,
            meta=meta,
            callback=callback
        )

    def start_requests(self):
        for line in open('names.txt'):
            for i in range(0, 6000, 60):
                yield self.new_request(line.strip() + ' 中药', i, self.parse)

    def parse(self, response):
        queryWord = response.request.meta['queryWord']
        json_response = json.loads(response.body)
        items = []
        for data in json_response['data']:
            print data
            if 'middleURL' in data:
                item = CustomItem()
                item['url'] = data['middleURL']
                item['word'] = queryWord.split()[0]
                items.append(item)
        return items
    #             yield scrapy.Request(
    #                 data['middleURL'],
    #                 method='GET',
    #                 callback=self.parse1)
    # def parse1(self, response):

        # urllib.urlretrieve(data['middleURL'], str(uuid.uuid4()) + '.jpg')
